using System;
using ValidadorDeSenha.Aplication;
using Xunit;

namespace ValidadorDeSenha.Test
{
    public class ValidadorDeSenhaTests
    {
        private readonly LoginAplication _senhaAplication;

        public ValidadorDeSenhaTests(LoginAplication senhaAplication)
        {
            _senhaAplication = senhaAplication;
        }

        [Theory]
        [InlineData("",false)]
        [InlineData("aa",false)]
        [InlineData("ab", false)]
        [InlineData("AAAbbbCc", false)]
        [InlineData("AbTp9!foo", false)]
        [InlineData("AbTp9!foA", false)]
        [InlineData("AbTp9 fok", false)]
        [InlineData("AbTp9!fok", true)]
        public void DeveriaValidarSenha(string senha, bool resultadoEsperado)
        {
            var resultado = _senhaAplication.ValidarSenha(senha);

            Assert.Equal(resultado.Sucesso, resultadoEsperado);
        }
    }
}
