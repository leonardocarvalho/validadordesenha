﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using ValidadorDeSenha.Aplication;

namespace ValidadorDeSenha.Test
{
    class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<LoginAplication, LoginAplication>();
        }
    }
}
