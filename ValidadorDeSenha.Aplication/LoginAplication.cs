﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ValidadorDeSenha.Domain;

namespace ValidadorDeSenha.Aplication
{
    public class LoginAplication
    {
        const string verificaTamanhoDaString = @"(.){9,}";
        const string encontraTodosOsDigitosDaString = @"[0-9]";
        const string encontraTodosOsCaracteresMinusculos = @"[a-z]";
        const string encontraTodosOsCaracteresMaiusculos = @"[A-Z]";
        const string encontraTodosOsCaracteresEspecial = @"[!@#$%^&*()-+]";
        const string encontraCaracteresRepetidos = @"(.).*(\1)";
        const string encontraEspacos = @"(\s)";
        public ResultadoValidacao ValidarSenha(string senha)
        {
            var resultadoValidacao = new ResultadoValidacao();

            ExecutaRegex(senha, ref resultadoValidacao, verificaTamanhoDaString, $"senha deve possuir 9 ou mais caracteres.", true);
            ExecutaRegex(senha, ref resultadoValidacao, encontraTodosOsDigitosDaString, $"senha deve possuir pelo menos 1 digito.", true);
            ExecutaRegex(senha, ref resultadoValidacao, encontraTodosOsCaracteresMinusculos, $"senha deve possuir pelo menos 1 caractere minusculo.", true);
            ExecutaRegex(senha, ref resultadoValidacao, encontraTodosOsCaracteresMaiusculos, $"senha deve possuir pelo menos 1 caractere maiusculo.", true);
            ExecutaRegex(senha, ref resultadoValidacao, encontraTodosOsCaracteresEspecial, $"senha deve possuir pelo menos 1 caractere especial.", true);
            ExecutaRegex(senha, ref resultadoValidacao, encontraCaracteresRepetidos, $"senha não pode possuir o mesmo caractere mais de uma vez.", false);
            ExecutaRegex(senha, ref resultadoValidacao, encontraEspacos, $"senha não pode conter espaços.", false);

            return resultadoValidacao;
        }

        protected void ExecutaRegex(string senha, ref ResultadoValidacao resultadoValidacao, string padraoRegex, string mensagemErro, bool resultadoEsperadoRegex)
        {
            Match m = Regex.Match(senha, padraoRegex);

            if (m.Success != resultadoEsperadoRegex)
            {
                resultadoValidacao.ErroValidacao(mensagemErro);
            }
        }

    }
}
