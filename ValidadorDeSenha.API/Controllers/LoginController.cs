﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ValidadorDeSenha.Aplication;
using ValidadorDeSenha.Domain;

namespace ValidadorDeSenha.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly LoginAplication _senhaAplication;
        public LoginController(LoginAplication senhaAplication)
        {
            _senhaAplication = senhaAplication;
        }
        [HttpPost("validarSenha")]
        public IActionResult ValidarSenha(Login login)
        {
            try
            {
                var result = _senhaAplication.ValidarSenha(login.Senha);

                if (result.Sucesso)
                    return Ok(result);

                return BadRequest(result);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}
