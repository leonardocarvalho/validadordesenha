**ValidadorDeSenha.API**

Api para validar senha com as seguintes regras:

Nove ou mais caracteres

Ao menos 1 dígito

Ao menos 1 letra minúscula

Ao menos 1 letra maiúscula

Ao menos 1 caractere especial

Não possuir caracteres repetido dentro do conjunto

Não conter espaços

Considere como especial os seguintes caracteres: !@#$%^&*()-+

**Como executar**


Executar o comando dotnet run no CMD dentro da pasta ValidadorDeSenha.API

abrir a pagina http://localhost:5001/swagger/

clicar em cima do endpoint e em seguida no botão "try it out"

preecher o JSON com a senha e clicar em executar

**Tecnologias**
.net core 3.1

swagger

xunit

**Arquitetura**

Divisão em 3 camadas: API,Aplication e Domain;

Implementação de teste unitario;

**Solução**

Realiza a validação da senha com Regex;

Retorna um objeto com 2 propriedades: Sucesso e Mensagens.
