﻿using System;
using System.Collections.Generic;

namespace ValidadorDeSenha.Domain
{
    public class ResultadoValidacao
    {
        public ResultadoValidacao()
        {
            Sucesso = true;
            Mensagens = new List<string>();
        }
        public bool Sucesso { get; set; }
        public List<string> Mensagens { get; set; }


        public void ErroValidacao(string mensagem)
        {
            Sucesso = false;
            Mensagens.Add(mensagem);
        }
    }

}
